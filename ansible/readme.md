# Configuration de l'infrastructure

## Inventaire

L'inventaire est automatiquement generé par Terraform.

## Variables

Les variables sont automatiquement generées par Terraform.

## Requis

Recuperer la cle SSH depuis AWS Secret manager et la placer dans ansible/{env}/examplekey.pm

### On récupere sa valeur depuis AWS secret manager et on la stock dans un fichier en local:

```bash
aws secretsmanager get-secret-value --secret-id morningnews_front_private_key --region eu-north-1 --query SecretString --output text > morningnews_front_key_pair.pem
```

### On restreint les droit du nouveau fichier enregistré:

```bash
chmod 400 "morningnews_front_key_pair.pem"
```


Besoin de supprimer la clé immediatement ?

```bash
aws secretsmanager delete-secret \
    --secret-id private_key_secret \
    --force-delete-without-recovery \
    --region eu-north-1
```

