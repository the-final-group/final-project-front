output "vpc_id" {
  value       = aws_instance.morningnews_vm.id
  description = "The ID of the VPC created."
}

output "public_instance_ip" {
  value       = aws_instance.morningnews_vm.public_ip
  description = "Public IP address of the public EC2 instances."
}

output "instance_dns" {
  value       = ovh_domain_zone_record.sub_morningnews
  description = "DNS config."
}

