/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('Visit the frontend url', () => {

  it('Visits and click on user icon', () => {

    // Visite l'URL du frontend
    cy.visit(Cypress.env('frontendUrl'))

    //Click sur l'icone utilisateur
    cy.get('.fa-user').click()

    // Verifie que la presence du mot "Sign-up"
    cy.contains('Sign-up')
  })

})
